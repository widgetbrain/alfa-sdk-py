import pytest
import wb_parameter_handler.main as wb_parameter_handler

from alfa_sdk.common.session import Session
from alfa_sdk.common.stores import AuthStore, ConfigStore


AuthStore.purge()
ConfigStore.purge()


@pytest.fixture(scope="session")
def credentials():
    client_id = "/alfa/development/user/1683/auth0/clientId"
    client_secret = "/alfa/development/user/1683/auth0/clientSecret"
    test_params = wb_parameter_handler.load_parameters([client_id, client_secret], path="/tmp/simple.cache")

    return {
        "client_id": test_params[client_id],
        "client_secret": test_params[client_secret],
    }


@pytest.fixture(scope="session")
def session(credentials):
    return Session(credentials, alfa_env="dev")


@pytest.fixture(scope="session")
def session_meta_local(credentials):
    sess = Session(credentials, alfa_env="dev")
    sess.context["__RUN_LOCAL__"] = True
    return sess
