import os
from alfa_sdk.common.stores import BaseStore, DEFAULT_GROUP


class TestDataStore:
    def test_file_path(self):
        path = StubStore.get_file_path()
        assert StubStore.FILE_NAME in path

    def test_init(self):
        try:
            path = StubStore.get_file_path()
            assert os.path.exists(path) is False
            StubStore.set_value("initialized", "true")
            assert os.path.exists(path) is True

        except Exception as err:
            StubStore.purge()
            raise err

    def test_get_value(self):
        value = StubStore.get_value("initialized")
        assert value == "true"

    def test_get_value_boolean(self):
        value = StubStore.get_value("initialized", is_boolean=True)
        assert value is True

    def test_get_value_direct(self):
        store = StubStore.get_store()
        value = store[store.default_section]["initialized"]
        assert value == "true"

        store = StubStore.get_group()
        value = store["initialized"]
        assert value == "true"

    def test_get_value_default(self):
        value = StubStore.get_value("nonexistant_value", "default_value")
        assert value == "default_value"

    def test_set_value_default_group(self):
        StubStore.set_value("new_key", "new_value")
        value = StubStore.get_value("new_key", group=DEFAULT_GROUP)

        assert value == "new_value"

    def test_set_value_custom_group(self):
        StubStore.set_value("new_key", "another_new_value", group="new.group")
        value = StubStore.get_value("new_key", group="new.group")

        assert value == "another_new_value"

    def test_purge(self):
        path = StubStore.get_file_path()
        StubStore.purge()
        assert os.path.exists(path) is False


class StubStore(BaseStore):
    FILE_NAME = ".this-is-a-test-store"
