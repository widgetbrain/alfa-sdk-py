import pytest
import os
import requests

from alfa_sdk.common.base import BaseClient
from alfa_sdk.common.auth import Authentication
from alfa_sdk.common.session import Session, parse_response
from alfa_sdk.common.stores import AuthStore, ConfigStore
from alfa_sdk.common.exceptions import AuthenticationError, ResourceNotFoundError


class TestSession:
    class TestAuthentication:
        @pytest.fixture(scope="class")
        def auth(self, credentials):
            return Authentication(credentials, alfa_env="dev")

        def test_auth_token(self, auth):
            assert auth.token is not None
            assert auth.token.lower().startswith("bearer ") is True

        def test_auth_user_data(self, auth):
            assert auth.user["username"] == "wbtest"
            assert auth.user["userId"] is not None
            assert auth.user["teamId"] is not None
            assert auth.user["role"] is not None

        def test_auth_failure(self, credentials):
            with pytest.raises(AuthenticationError):
                Authentication(credentials, alfa_env="prod")

        def test_auth_credentials_environment(self, credentials):
            os.environ["ALFA_CLIENT_ID"] = credentials.get("client_id")
            os.environ["ALFA_CLIENT_SECRET"] = credentials.get("client_secret")
            auth = Authentication(alfa_env="dev")
            del os.environ["ALFA_CLIENT_ID"]
            del os.environ["ALFA_CLIENT_SECRET"]

            assert auth.token is not None
            assert auth.token.lower().startswith("bearer ") is True
            assert auth.user["username"] == "wbtest"

        def test_auth_credentials_store(self, credentials):
            AuthStore.set_values(
                {
                    "client_id": credentials.get("client_id"),
                    "client_secret": credentials.get("client_secret"),
                }
            )
            auth = Authentication(alfa_env="dev")
            AuthStore.purge()

            assert auth.token is not None
            assert auth.token.lower().startswith("bearer ") is True
            assert auth.user["username"] == "wbtest"

        class TestToken:
            def test_token_argument(self, auth):
                auth = Authentication({"token": auth.token}, alfa_env="dev")

                assert auth.token is not None
                assert auth.token.lower().startswith("bearer ") is True
                assert auth.user["username"] == "wbtest"

            def test_token_environment(self, auth):
                os.environ["ALFA_TOKEN"] = auth.token
                auth = Authentication(alfa_env="dev")
                del os.environ["ALFA_TOKEN"]

                assert auth.token is not None
                assert auth.token.lower().startswith("bearer ") is True
                assert auth.user["username"] == "wbtest"

            def test_token_environment_compatibility(self, auth):
                os.environ["ALFA_AUTH0_TOKEN"] = auth.token
                auth = Authentication(alfa_env="dev")
                del os.environ["ALFA_AUTH0_TOKEN"]

                assert auth.token is not None
                assert auth.token.lower().startswith("bearer ") is True
                assert auth.user["username"] == "wbtest"

            def test_token_store(self, auth):
                AuthStore.set_value("token", auth.token)
                auth = Authentication(alfa_env="dev")
                AuthStore.purge()

                assert auth.token is not None
                assert auth.token.lower().startswith("bearer ") is True
                assert auth.user["username"] == "wbtest"

    class TestSessionInitialization:
        def test_alfa_env_default(self, credentials):
            with pytest.raises(AuthenticationError):
                Session(credentials)

        def test_alfa_env_args(self, credentials):
            session = Session(credentials, alfa_env="dev")

            assert session.auth.token is not None
            assert session.auth.token.lower().startswith("bearer ") is True
            assert session.auth.user["username"] == "wbtest"

        def test_alfa_env_environment(self, credentials):
            os.environ["ALFA_ENV"] = "dev"
            session = Session(credentials)
            del os.environ["ALFA_ENV"]

            assert session.auth.token is not None
            assert session.auth.token.lower().startswith("bearer ") is True
            assert session.auth.user["username"] == "wbtest"

        def test_alfa_env_store(self, credentials):
            ConfigStore.set_value("alfa_env", "dev", group="alfa")
            session = Session(credentials)
            ConfigStore.purge()

            assert session.auth.token is not None
            assert session.auth.token.lower().startswith("bearer ") is True
            assert session.auth.user["username"] == "wbtest"

    class TestBaseClient:
        @pytest.fixture(scope="class")
        def client(self, session):
            return BaseClient(session=session)

        def test_request_core(self, client):
            client.session.request("get", "core", "/")

        def test_request_team(self, client):
            params = {"teamId": client.user["teamId"]}
            team = client.session.request("get", "core", "/api/Teams/getInfo", params=params)
            assert team["name"] == "AI Algorithms"

        def test_invoke_algorithm(self, client):
            algorithm_id = "bb1d784a-bc4d-4f8a-9805-25d8a38baa29"
            environment = "alfa-sdk"
            problem = {"value": 70}

            res = client.session.invoke(algorithm_id, environment, problem)
            assert res["out"] == 140

        def test_parse_error_no_token(self, client):
            with pytest.raises(AuthenticationError):
                url = client.session.endpoint.resolve("baas")
                res = requests.get(url)
                parse_response(res)

        def test_parse_error_no_resource(self, client):
            with pytest.raises(ResourceNotFoundError):
                client.session.request("get", "baas", "/api/AlgorithmEnvironments/getInfo/XXX")

        def test_context(self, session):
            context = {"token": session.auth.token, "alfaEnvironment": "dev"}
            client = BaseClient(context=context)

            params = {"teamId": client.user["teamId"]}
            team = client.session.request("get", "core", "/api/Teams/getInfo", params=params)

            assert client.user["username"] == "wbtest"
            assert team["name"] == "AI Algorithms"

        def test_context_none(self, session):
            os.environ["ALFA_TOKEN"] = session.auth.token
            os.environ["ALFA_ENV"] = "dev"
            client = BaseClient(context=None)
            del os.environ["ALFA_TOKEN"]
            del os.environ["ALFA_ENV"]

            params = {"teamId": client.user["teamId"]}
            team = client.session.request("get", "core", "/api/Teams/getInfo", params=params)

            assert client.user["username"] == "wbtest"
            assert team["name"] == "AI Algorithms"
