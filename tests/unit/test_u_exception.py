from alfa_sdk.common import exceptions


class TestExceptionStatus:
    def test_error_status_custom(self):
        # ACT
        error = exceptions.RequestError(url="http://foo.bar", error="some error", status=400)

        # ASSERT
        assert error.status == 400

    def test_error_status_builtin(self):
        # ACT
        error = exceptions.ResourceError(resource="some resource", error="some error")

        # ASSERT
        assert error.status == 400
