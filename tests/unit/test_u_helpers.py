import pytest
from alfa_sdk.common.session import Session
from alfa_sdk.common.helpers import EndpointHelper, VersionHelper
from alfa_sdk.common.exceptions import (
    UnknownServiceError,
    ServiceEnvironmentError,
    SemanticVersionError,
)


class TestResolveEndpoint:
    endpoint = EndpointHelper(alfa_env="prod")
    endpoint_dev = EndpointHelper(alfa_env="dev")

    def test_initialize_helper(self):
        endpoint = Session.initialize_endpoint_helper(alfa_env="test", alfa_id="quinyx")
        assert isinstance(endpoint, EndpointHelper)
        assert endpoint.alfa_env == "test"
        assert endpoint.alfa_id == "quinyx"

    def test_resolve_alfa_url(self):
        url = self.endpoint.resolve("core")
        assert url == "https://wb-core.widgetbrain.io"

    def test_resolve_alfa_url_path(self):
        url = self.endpoint.resolve("core", "/api")
        assert url == "https://wb-core.widgetbrain.io/api"

    def test_resolve_alfa_url_http(self):
        url = self.endpoint_dev.resolve("ais")
        assert url == "https://ais-dev.widgetbrain.io"

    def test_resolve_invalid_service(self):
        with pytest.raises(UnknownServiceError):
            self.endpoint.resolve("core_invalid")

    def test_resolve_invalid_service_environment(self):
        with pytest.raises(ServiceEnvironmentError):
            endpoint = EndpointHelper(alfa_env="invalid_env")
            endpoint.resolve("core")

    def test_resolve_non_public_alfa_url(self):
        endpoint = EndpointHelper(alfa_env="dev", alfa_id="quinyx")
        url = endpoint.resolve("baas")
        assert url == "https://alfa-baas-eu-central-1.web-test.quinyx.com"

    def test_resolve_non_public_alfa_url_region(self):
        endpoint = EndpointHelper(alfa_env="dev", alfa_id="quinyx", region="us-east-1")
        url = endpoint.resolve("baas")
        assert url == "https://alfa-baas-us-east-1.web-test.quinyx.com"


class TestVersionHelper:
    def test_version_error(self):
        with pytest.raises(SemanticVersionError):
            VersionHelper.get("first_version")

    def test_increment_version(self):
        version = VersionHelper.increment("0.1.1")
        assert str(version) == "0.1.2"

    def test_find_latest_version(self):
        versions = ["0.0.1", "0.0.2", "0.0.3", "0.1.0"]
        version = VersionHelper.latest(versions)
        assert str(version) == "0.1.0"
