from alfa_sdk.common.exceptions import RequestError
import json
import os
import urllib
from uuid import uuid4
from unittest.mock import patch, MagicMock
import pytest
import responses
from alfa_sdk.common.session import Session, parse_response


#


def mock_authentication():
    responses.add(
        responses.POST,
        "https://wb-core-development.widgetbrain.io/api/ApiKeyValidators/requestToken",
        status=200,
        json={"token_type": "bearer", "access_token": "A_REQUESTED_TOKEN"},
    )

    responses.add(
        responses.GET,
        "https://wb-core-development.widgetbrain.io/api/ApiKeyValidators/validateTokenForApp",
        status=200,
        json={"id": -1},
    )


def mock_config_store():
    config_store = MagicMock()
    config_store.get_group = MagicMock(
        return_value={
            "alfa_env": "STORE_ALFA_ENV",
            "alfa_id": "STORE_ALFA_ID",
            "platform_region": "STORE_PLATFORM_REGION",
        }
    )
    return config_store


def mock_endpoint_helper():
    def mock_endpoint_resolve(service, path="", **kwargs):
        if service == "core":
            return "https://wb-core-development.widgetbrain.io{}".format(path)

    endpoint_helper = MagicMock()
    endpoint_helper.resolve = MagicMock(side_effect=mock_endpoint_resolve)
    return MagicMock(return_value=endpoint_helper)


@pytest.fixture(scope="function")
@responses.activate
@patch("alfa_sdk.common.session.ConfigStore", new=mock_config_store())
@patch("alfa_sdk.common.session.EndpointHelper", new=mock_endpoint_helper())
@patch("alfa_sdk.common.auth.EndpointHelper", new=mock_endpoint_helper())
def mocked_session():
    mock_authentication()

    credentials = {"client_id": "SOME_CLIENT_ID", "client_secret": "SOME_CLIENT_SECRET"}
    session = Session(credentials)
    return session


#


class TestSessionInitialize:
    @pytest.fixture(scope="function", autouse=True)
    def setup_teardown(self):

        yield

        if "ALFA_TOKEN" in os.environ:
            del os.environ["ALFA_TOKEN"]
        if "ALFA_ENV" in os.environ:
            del os.environ["ALFA_ENV"]
        if "ALFA_CONTEXT" in os.environ:
            del os.environ["ALFA_CONTEXT"]
        if "ALFA_ID" in os.environ:
            del os.environ["ALFA_ID"]
        if "PLATFORM_REGION" in os.environ:
            del os.environ["PLATFORM_REGION"]

    #

    @responses.activate
    def test_prefer_client_id_and_secret_over_token(self):
        # ARRANGE
        mock_authentication()

        os.environ["ALFA_ENV"] = "development"
        os.environ["ALFA_TOKEN"] = "SOME_TOKEN"
        credentials = {"client_id": "SOME_CLIENT_ID", "client_secret": "SOME_CLIENT_SECRET"}

        # ACT
        Session(credentials)

        # ASSERT
        calls_to_request_token = [
            call
            for call in responses.calls
            if "api/apikeyvalidators/requesttoken" in call.request.url.lower()
        ]
        assert len(calls_to_request_token) == 1

        credentials_used_to_request_token = urllib.parse.parse_qs(
            calls_to_request_token[0].request.body
        )
        assert credentials_used_to_request_token["clientId"][0] == credentials["client_id"]
        assert credentials_used_to_request_token["clientSecret"][0] == credentials["client_secret"]

    @responses.activate
    def test_use_token_when_no_client_id_secret_provided(self):
        # ARRANGE
        mock_authentication()

        os.environ["ALFA_ENV"] = "development"
        os.environ["ALFA_TOKEN"] = "SOME_TOKEN"
        credentials = {}

        # ACT
        Session(credentials)

        # ASSERT
        calls_to_request_token = [
            call
            for call in responses.calls
            if "api/apikeyvalidators/requesttoken" in call.request.url.lower()
        ]
        assert len(calls_to_request_token) == 0

    @responses.activate
    def test_get_credentials(self):
        # ARRANGE
        mock_authentication()

        os.environ["ALFA_ENV"] = "development"
        os.environ["ALFA_TOKEN"] = "SOME_TOKEN"
        credentials = {}

        # ACT
        session = Session(credentials)
        token = session.auth.get_token()
        credentials = session.auth.get_credentials()

        # ASSERT
        assert token == os.environ["ALFA_TOKEN"]
        assert credentials["cookie"] is None
        assert credentials["token"] == os.environ["ALFA_TOKEN"]

    @responses.activate
    def test_append_neo_session_id_as_cookie(self):
        # ARRANGE
        mock_authentication()

        os.environ["ALFA_ENV"] = "development"
        credentials = {"token": "bearer A_REQUESTED_TOKEN"}
        context = {"neoSessionId": "SESSIONID"}

        # ACT
        session = Session(credentials, context=context)
        auth = session.auth

        # ASSERT
        assert "SESSIONID=SESSIONID" in auth.cookie
        assert "cookie" in session.http_session.headers
        assert "SESSIONID=SESSIONID" in session.http_session.headers["cookie"]

    @responses.activate
    def test_append_neo_session_id_to_existing_cookie(self):
        # ARRANGE
        mock_authentication()

        os.environ["ALFA_ENV"] = "development"
        credentials = {"cookie": "test=test; SESSIONID=SOMETHING; XSESSIONID=XSESSIONID"}
        context = {"neoSessionId": "SESSIONID"}

        # ACT
        session = Session(credentials, context=context)
        auth = session.auth

        # ASSERT
        assert "SESSIONID=SESSIONID" in auth.cookie
        assert "cookie" in session.http_session.headers
        assert "SESSIONID=SESSIONID" in session.http_session.headers["cookie"]
        assert "XSESSIONID=XSESSIONID" in session.http_session.headers["cookie"]

    @responses.activate
    @patch("alfa_sdk.common.session.ConfigStore", new=mock_config_store())
    @patch("alfa_sdk.common.session.EndpointHelper", new=mock_endpoint_helper())
    @patch("alfa_sdk.common.auth.EndpointHelper", new=mock_endpoint_helper())
    def test_prioritize_client_specified_configuration(self):
        # ARRANGE
        mock_authentication()

        os.environ["ALFA_ID"] = "ENVVAR_ALFA_ID"
        os.environ["PLATFORM_REGION"] = "ENVVAR_PLATFORM_REGION"
        os.environ["ALFA_ENV"] = "ENVVAR_ALFA_ENV"
        os.environ["ALFA_CONTEXT"] = json.dumps(
            {
                "alfaEnvironment": "ENVVAR_CONTEXT_ALFA_ENV",
                "alfaID": "ENVVAR_CONTEXT_ALFA_ID",
                "platformRegion": "ENVVAR_CONTEXT_PLATFORM_REGION",
            }
        )

        client_alfa_env = "CLIENT_SPECIFIED_ALFA_ENV"
        client_alfa_id = "CLIENT_SPECIFIED_ALFA_ID"
        client_platform_region = "CLIENT_SPECIFIED_PLATFORM_REGION"

        # ACT
        credentials = {"client_id": "SOME_CLIENT_ID", "client_secret": "SOME_CLIENT_SECRET"}
        session = Session(
            credentials,
            alfa_env=client_alfa_env,
            alfa_id=client_alfa_id,
            platform_region=client_platform_region,
        )

        # ASSERT
        assert session.alfa_env == client_alfa_env
        assert session.alfa_id == client_alfa_id
        assert session.region == client_platform_region

    @responses.activate
    @patch("alfa_sdk.common.session.ConfigStore", new=mock_config_store())
    @patch("alfa_sdk.common.session.EndpointHelper", new=mock_endpoint_helper())
    @patch("alfa_sdk.common.auth.EndpointHelper", new=mock_endpoint_helper())
    def test_prioritize_env_var_configuration(self):
        # ARRANGE
        mock_authentication()

        os.environ["ALFA_ID"] = "ENVVAR_ALFA_ID"
        os.environ["PLATFORM_REGION"] = "ENVVAR_PLATFORM_REGION"
        os.environ["ALFA_ENV"] = "ENVVAR_ALFA_ENV"
        os.environ["ALFA_CONTEXT"] = json.dumps(
            {
                "alfaEnvironment": "ENVVAR_CONTEXT_ALFA_ENV",
                "alfaID": "ENVVAR_CONTEXT_ALFA_ID",
                "platformRegion": "ENVVAR_CONTEXT_PLATFORM_REGION",
            }
        )

        # ACT
        credentials = {"client_id": "SOME_CLIENT_ID", "client_secret": "SOME_CLIENT_SECRET"}
        session = Session(credentials)

        # ASSERT
        assert session.alfa_env == os.environ["ALFA_ENV"]
        assert session.alfa_id == os.environ["ALFA_ID"]
        assert session.region == os.environ["PLATFORM_REGION"]

    @responses.activate
    @patch("alfa_sdk.common.session.ConfigStore", new=mock_config_store())
    @patch("alfa_sdk.common.session.EndpointHelper", new=mock_endpoint_helper())
    @patch("alfa_sdk.common.auth.EndpointHelper", new=mock_endpoint_helper())
    def test_prioritize_env_var_context_configuration(self):
        # ARRANGE
        mock_authentication()

        os.environ["ALFA_CONTEXT"] = json.dumps(
            {
                "alfaEnvironment": "ENVVAR_CONTEXT_ALFA_ENV",
                "alfaID": "ENVVAR_CONTEXT_ALFA_ID",
                "platformRegion": "ENVVAR_CONTEXT_PLATFORM_REGION",
            }
        )

        # ACT
        credentials = {"client_id": "SOME_CLIENT_ID", "client_secret": "SOME_CLIENT_SECRET"}
        session = Session(credentials)

        # ASSERT
        context = json.loads(os.environ["ALFA_CONTEXT"])
        assert session.alfa_env == context["alfaEnvironment"]
        assert session.alfa_id == context["alfaID"]
        assert session.region == context["platformRegion"]

    @responses.activate
    @patch("alfa_sdk.common.session.ConfigStore", new=mock_config_store())
    @patch("alfa_sdk.common.session.EndpointHelper", new=mock_endpoint_helper())
    @patch("alfa_sdk.common.auth.EndpointHelper", new=mock_endpoint_helper())
    def test_prioritize_config_store_configuration(self):
        # ARRANGE
        mock_authentication()

        # ACT
        credentials = {"client_id": "SOME_CLIENT_ID", "client_secret": "SOME_CLIENT_SECRET"}
        session = Session(credentials)

        # ASSERT
        assert session.alfa_env == "STORE_ALFA_ENV"
        assert session.alfa_id == "STORE_ALFA_ID"
        assert session.region == "STORE_PLATFORM_REGION"

    @responses.activate
    @patch("alfa_sdk.common.session.ConfigStore", new=MagicMock())
    @patch("alfa_sdk.common.session.EndpointHelper", new=mock_endpoint_helper())
    @patch("alfa_sdk.common.auth.EndpointHelper", new=mock_endpoint_helper())
    def test_default_configuration(self):
        # ARRANGE
        mock_authentication()

        # ACT
        credentials = {"client_id": "SOME_CLIENT_ID", "client_secret": "SOME_CLIENT_SECRET"}
        session = Session(credentials)

        # ASSERT
        assert session.alfa_env == "prod"
        assert session.alfa_id == "public"
        assert session.region == "eu-central-1"

    @pytest.fixture(scope="function", autouse=True)
    def clean_context(self):
        yield
        if "ALFA_CONTEXT" in os.environ:
            del os.environ["ALFA_CONTEXT"]

    @responses.activate
    def test_auth_token(self):
        # Arrange
        token = str(uuid4())
        os.environ["ALFA_CONTEXT"] = json.dumps({"token": token, "alfaEnvironment": "dev"})

        responses.add(
            responses.GET,
            "https://wb-core-development.widgetbrain.io/api/ApiKeyValidators/validateTokenForApp",
            match_querystring=False,
            status=200,
            json={},
        )

        responses.add(
            responses.GET,
            "https://baas-development.widgetbrain.io/api/foo",
            status=200,
            json={},
        )

        # Act
        session = Session()
        session.request("get", "baas", "/api/foo", json={})

        # Assert
        assert len(responses.calls) == 2
        request_baas = responses.calls[1].request

        assert "wb-authorization" in request_baas.headers
        assert "cookie" not in request_baas.headers
        assert request_baas.headers["wb-authorization"] == token

    @responses.activate
    def test_auth_cookie(self):
        # Arrange
        cookie = "auth-test=macaroon-key; SESSIONID=session-id;"
        os.environ["ALFA_CONTEXT"] = json.dumps({"cookie": cookie, "alfaEnvironment": "dev"})

        responses.add(
            responses.GET,
            "https://wb-core-development.widgetbrain.io/api/ApiKeyValidators/validateTokenForApp",
            match_querystring=False,
            status=200,
            json={},
        )

        responses.add(
            responses.GET,
            "https://baas-development.widgetbrain.io/api/foo",
            content_type="application/json",
            status=200,
            json={},
        )

        # Act
        session = Session()
        session.request("get", "baas", "/api/foo", json={})

        # Assert
        assert len(responses.calls) == 2
        request_baas = responses.calls[1].request
        assert "wb-authorization" not in request_baas.headers
        assert "cookie" in request_baas.headers
        assert request_baas.headers["cookie"] == cookie

    @responses.activate
    def test_auth_token_priority(self):
        # Arrange
        token_direct = str(uuid4())
        token_context = str(uuid4())
        os.environ["ALFA_CONTEXT"] = json.dumps({"token": token_context, "alfaEnvironment": "dev"})

        responses.add(
            responses.GET,
            "https://wb-core-development.widgetbrain.io/api/ApiKeyValidators/validateTokenForApp",
            match_querystring=False,
            status=200,
            json={},
        )

        responses.add(
            responses.GET,
            "https://baas-development.widgetbrain.io/api/foo",
            status=200,
        )

        # Act
        session = Session({"token": token_direct})
        session.request("get", "baas", "/api/foo")

        # Assert
        assert len(responses.calls) == 2
        request_baas = responses.calls[1].request

        assert "wb-authorization" in request_baas.headers
        assert "cookie" not in request_baas.headers
        assert request_baas.headers["wb-authorization"] == token_direct

    @responses.activate
    def test_auth_append_neo_session_id(self):
        # ARRANGE
        mock_authentication()
        responses.add(
            responses.GET,
            "https://baas-development.widgetbrain.io/api/foo",
            status=200,
        )

        os.environ["ALFA_ENV"] = "development"
        credentials = {"token": "bearer A_REQUESTED_TOKEN"}
        context = {"neoSessionId": "SESSIONID"}

        # ACT
        session = Session(credentials, context=context)
        session.request(method="GET", service="baas", path="/api/foo")

        # ASSERT
        assert len(responses.calls) == 2
        request_baas = responses.calls[1].request

        assert "wb-authorization" in request_baas.headers
        assert "cookie" in request_baas.headers
        assert "SESSIONID=SESSIONID" in request_baas.headers["cookie"]


class TestSessionInvoke:
    @pytest.fixture(scope="function")
    def invoke_session(self, mocked_session):
        mocked_session.invoke_algorithm = MagicMock()
        mocked_session.invoke_integration = MagicMock()
        return mocked_session

    def test_default_invoke_algorithm(self, invoke_session):
        # Arrange
        function_id = str(uuid4())
        environment = "foo"
        problem = {}

        # Act
        invoke_session.invoke(function_id, environment, problem)

        # Assert
        assert invoke_session.invoke_algorithm.call_count == 1
        assert invoke_session.invoke_integration.call_count == 0

    def test_arg_invoke_integration(self, invoke_session):
        # Arrange
        function_id = str(uuid4())
        environment = "foo"
        problem = {}
        function_type = "integration"

        # Act
        invoke_session.invoke(function_id, environment, problem, function_type=function_type)

        # Assert
        assert invoke_session.invoke_algorithm.call_count == 0
        assert invoke_session.invoke_integration.call_count == 1


class TestSessionRequest:
    @patch("alfa_sdk.common.session.parse_response")
    @patch("requests.sessions.Session.request")
    def test_session_request(self, mock_request, mock_parse, mocked_session):
        mock_request.return_value = {}
        mock_parse.return_value = {}

        # Act
        mocked_session.request("get", "core", path="/api/caches/test")

        # Assert
        mock_parse.assert_called_once()
        mock_request.assert_called_once()
        mock_request.assert_called_with(
            "get",
            "https://wb-core-development.widgetbrain.io/api/caches/test",
        )

    @patch("alfa_sdk.common.session.parse_response")
    @patch("requests.sessions.Session.request")
    def test_session_request_default_options(self, mock_request, mock_parse, mocked_session):
        mock_request.return_value = {}
        mock_parse.return_value = {}

        # Act
        mocked_session.set_default({"service": "core", "prefix": "/api/caches"})
        mocked_session.request("get", path="/test")
        mocked_session.set_default({})

        # Assert
        mock_parse.assert_called_once()
        mock_request.assert_called_once()
        mock_request.assert_called_with(
            "get",
            "https://wb-core-development.widgetbrain.io/api/caches/test",
        )


class TestSessionParseResponse:
    def test_non_string_error_message(self):
        # ARRANGE
        res = MagicMock()
        res.json = MagicMock(
            return_value={
                "error": {
                    "message": [
                        {
                            "field": "requests[0].forecastDataPayload[37].data",
                            "message": "error.forecast.requests.forecastDataPayLoad.data.empty",
                            "additionalErrorParameters": {},
                        },
                        {
                            "field": "requests[0].forecastDataPayload[58].data",
                            "message": "error.forecast.requests.forecastDataPayLoad.data.empty",
                            "additionalErrorParameters": {},
                        },
                    ]
                }
            }
        )

        # ACT & ASSERT
        with pytest.raises(RequestError):
            parse_response(res)
