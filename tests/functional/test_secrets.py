import pytest
import uuid

from alfa_sdk import SecretsClient


class TestSecrets:
    @pytest.fixture(scope="class")
    def client(self, session):
        return SecretsClient(session=session)

    @pytest.fixture(scope="class")
    def name(self):
        return generate_name()

    def test_store_value(self, client, name):
        res = client.store_value(name, "test_value")
        assert res["name"] == name
        assert res["description"] == None
        assert res["teamId"] == client.user["teamId"]

    def test_store_value_replace(self, client, name):
        res = client.store_value(name, "new_value", description="an updated value!")
        assert res["name"] == name
        assert res["description"] == "an updated value!"

    def test_remove_value(self, client, name):
        res = client.remove_value(name)
        assert type(res) is dict

    class TestSecretsFetch:
        @pytest.fixture(scope="class")
        def secrets(self, client):
            secret1 = client.store_value(generate_name(), "test_value_1")
            secret2 = client.store_value(generate_name(), "test_value_2")

            secrets = [secret1, secret2]
            yield secrets

            client.remove_value(secret1["name"])
            client.remove_value(secret2["name"])

        def test_get_names(self, client, secrets):
            res = client.list_names()
            names = [x["name"] for x in res]
            assert (secrets[0]["name"] in names) is True
            assert (secrets[1]["name"] in names) is True

        def test_get_value(self, client, secrets):
            res1 = client.fetch_value(secrets[0]["name"])
            res2 = client.fetch_value(secrets[1]["name"])
            assert res1 == "test_value_1"
            assert res2 == "test_value_2"

        def test_get_values(self, client, secrets):
            res = client.fetch_values([x["name"] for x in secrets])
            assert res[secrets[0]["name"]] == "test_value_1"
            assert res[secrets[1]["name"]] == "test_value_2"


#


def generate_name():
    return "{}-{}".format("test", str(uuid.uuid4())).replace("-", "/")
