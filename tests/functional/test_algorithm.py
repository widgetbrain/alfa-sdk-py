from datetime import datetime
import pytest
import os
import json
import uuid
import responses

from unittest.mock import patch

from alfa_sdk import AlgorithmClient, SecretsClient
from alfa_sdk.resources import Algorithm, AlgorithmEnvironment
from alfa_sdk.common.exceptions import RequestError, ResourceDeletionError


class TestAlgorithm:
    @pytest.fixture(scope="class")
    def client(self, session):
        return AlgorithmClient(session=session)

    @pytest.fixture(scope="class")
    def constants(self):
        return {
            "algorithm_id": "bb1d784a-bc4d-4f8a-9805-25d8a38baa29",
            "environment_id": "9a882618-336e-4988-b85c-dc98b7e13c4f:bb1d784a-bc4d-4f8a-9805-25d8a38baa29:alfa-sdk",
            "environment_name": "alfa-sdk",
            "environment_id_new": "9a882618-336e-4988-b85c-dc98b7e13c4f:bb1d784a-bc4d-4f8a-9805-25d8a38baa29:alfa-sdk-new",
            "environment_name_new": "alfa-sdk-new",
            "allowed_team_id": str(uuid.uuid4()),
            "allowed_algorithm_id": str(uuid.uuid4()),
        }

    @pytest.fixture(scope="class")
    def cclient(self, constants, session):
        os.environ["ALFA_CONTEXT"] = json.dumps(
            {
                "alfaEnvironment": "dev",
                "token": session.auth.token,
                "environmentId": constants.get("environment_id"),
                "userId": session.auth.user["userId"],
                "teamId": session.auth.user["teamId"],
                "algorithmRunId": str(uuid.uuid4()),
            }
        )
        client = AlgorithmClient()
        yield client
        del os.environ["ALFA_CONTEXT"]

    def test_secrets_initialized(self, client):
        assert type(client.secrets) is SecretsClient

    def test_list_algorithms(self, client, constants):
        algorithms = client.list_algorithms()
        ids = [x["id"] for x in algorithms]
        assert (constants["algorithm_id"] in ids) is True

    def test_list_environments(self, client, constants):
        environments = client.list_environments(constants["algorithm_id"])
        ids = [x["id"] for x in environments]
        assert (constants["environment_id"] in ids) is True

    def test_list_releases(self, client, constants):
        releases = client.list_releases(constants["environment_id"])
        environment_ids = [x["environmentId"] for x in releases]
        assert (constants["environment_id"] in environment_ids) is True

    @patch("alfa_sdk.resources.algorithm.Algorithm._fetch_data")
    def test_get_algorithm_from_allowed_team(self, get_algorithm, client, constants):
        # ARRANGE
        get_algorithm.return_value = {
            "id": constants["algorithm_id"],
            "name": "Test Algorithm",
            "description": "Algorithm For Package Tests",
            "teamId": constants["allowed_team_id"],
        }

        # ACT
        algorithm = client.get_algorithm(
            constants["algorithm_id"], team_id=constants["allowed_team_id"]
        )

        # ASSERT
        assert algorithm.team_id == constants["allowed_team_id"]

    @patch("alfa_sdk.resources.algorithm.Algorithm._fetch_data")
    @patch("alfa_sdk.resources.algorithm.AlgorithmEnvironment._fetch_data")
    def test_get_environment_from_allowed_team(
        self, get_environment, get_algorithm, client, constants
    ):
        # ARRANGE
        get_algorithm.return_value = {
            "id": constants["algorithm_id"],
            "name": "Test Algorithm",
            "description": "Algorithm For Package Tests",
            "teamId": constants["allowed_team_id"],
        }
        get_environment.return_value = {}
        environment_name = "any"

        # ACT
        algorithm = client.get_algorithm(
            constants["algorithm_id"], team_id=constants["allowed_team_id"]
        )
        algorithm_environment = algorithm.get_environment(environment_name)

        # ASSERT
        assert algorithm_environment.id == "{}:{}:{}".format(
            constants["allowed_team_id"], constants["algorithm_id"], environment_name
        )

    @patch("alfa_sdk.algorithm.AlgorithmClient.list_algorithms")
    def test_list_algorithms_from_allowed_team(self, list_algorithms, client, constants):
        # ARRANGE
        list_algorithms.return_value = [
            {
                "id": constants["allowed_algorithm_id"],
                "name": "Test Algorithm",
                "description": "Algorithm For Package Tests",
                "teamId": constants["allowed_team_id"],
            }
        ]
        temp_team_id = client.team_id
        client.team_id = constants["allowed_algorithm_id"]

        # ACT
        algorithms = client.list_algorithms()

        # ASSERT
        assert algorithms[0]["id"] == constants["allowed_algorithm_id"]
        assert algorithms[0]["teamId"] == constants["allowed_team_id"]

        # RESET
        client.team_id = temp_team_id

    @responses.activate
    def test_store_kpi(self, cclient, constants):
        # Arrange
        entity = "alfa.sdk.test"
        kpis = {"name": "test", "value": 42}

        responses.add(
            responses.GET,
            "https://baas-development.widgetbrain.io/api/AlgorithmEnvironments/getInfo/9a882618-336e-4988-b85c-dc98b7e13c4f:bb1d784a-bc4d-4f8a-9805-25d8a38baa29:alfa-sdk",
            status=200,
            json=constants,
        )

        def request_callback(request):
            assert request.method == "POST"
            assert request.url == "https://watch-dev.widgetbrain.io/api/kpis/add"

            body = json.loads(request.body)
            assert body["algorithmEnvironmentId"] == constants["environment_id"]
            assert body["algorithmRunId"] == cclient.session.context["algorithmRunId"]
            assert isinstance(body["data"], list)
            assert len(body["data"]) == 1

            res = []
            for entry in body["data"]:
                assert entry["name"] == kpis["name"]
                assert entry["value"] == kpis["value"]
                assert entry["time"] is not None
                assert entry["entity"] == entity
                res.append(
                    {
                        "algorithmEnvironmentId": body["algorithmEnvironmentId"],
                        "algorithmRunId": body["algorithmRunId"],
                        "name": entry["name"],
                        "value": entry["value"],
                        "time": entry["time"],
                        "entity": entry["entity"],
                        "period": "",
                    }
                )
            return (200, {}, json.dumps(res))

        responses.add_callback(
            responses.POST,
            "https://watch-dev.widgetbrain.io/api/kpis/add",
            callback=request_callback,
            content_type="application/json",
        )

        # Act
        stored_kpis = cclient.store_kpi(kpis, entity)

        # Assert
        assert isinstance(stored_kpis, list)
        assert len(stored_kpis) == 1
        for kpi in stored_kpis:
            assert kpi["algorithmEnvironmentId"] == constants["environment_id"]
            assert kpi["algorithmRunId"] == cclient.session.context["algorithmRunId"]
            assert kpi["entity"] == entity
            assert kpi["name"] == kpis["name"]
            assert kpi["value"] == kpis["value"]
            assert kpi["period"] == ""
            assert "time" in kpi

    class TestAlgorithmClient:
        def test_invoke(self, client, constants):
            algorithm = constants["algorithm_id"]
            environment = constants["environment_name"]
            problem = {"value": 70}

            res = client.invoke(algorithm, environment, problem)
            assert res["out"] == 140

        def test_invoke_params_return_reference(self, client, constants):
            algorithm = constants["algorithm_id"]
            environment = constants["environment_name"]
            problem = {"value": 70}

            res = client.invoke(algorithm, environment, problem, return_reference=True, parse=False)

            body = json.loads(res.request.body)
            assert not body["dryRunOptions"]
            assert "returnReference" in body
            assert body["returnReference"] == True

        def test_invoke_params_return_holding_response(self, client, constants):
            algorithm = constants["algorithm_id"]
            environment = constants["environment_name"]
            problem = {"value": 70}

            res = client.invoke(
                algorithm,
                environment,
                problem,
                return_holding_response=True,
                parse=False,
            )

            body = json.loads(res.request.body)
            assert not body["dryRunOptions"]
            assert "returnHoldingResponse" in body
            assert body["returnHoldingResponse"] == True

    class TestAlgorithmResource:
        @pytest.fixture(scope="class")
        def algorithm(self, client, constants):
            return client.get_algorithm(constants["algorithm_id"])

        def test_algorithm_info(self, algorithm, constants):
            assert type(algorithm) is Algorithm
            assert algorithm.id == constants["algorithm_id"]
            assert algorithm.name == "Test Algorithm"
            assert algorithm.type == "Test"

        def test_algorithm_environments(self, algorithm, constants):
            environments = algorithm.list_environments()
            names = [x["name"] for x in environments]
            assert (constants["environment_name"] in names) is True

        def test_algorithm_invoke(self, algorithm, constants):
            environment = constants["environment_name"]
            problem = {"value": 70}

            res = algorithm.invoke(environment, problem)
            assert res["out"] == 140

    class TestEnvironmentResource:
        @pytest.fixture(scope="class")
        def environment(self, client, constants):
            return client.get_environment(constants["environment_id"])

        def test_environment_info(self, environment, constants):
            assert type(environment) is AlgorithmEnvironment
            assert environment.id == constants["environment_id"]
            assert environment.name == constants["environment_name"]
            assert environment.algorithm_id == constants["algorithm_id"]

        def test_environment_registrations(self, environment):
            registrations = environment.list_registrations()
            assert registrations.get("invoke") is not None

        def test_get_algorithm(self, environment, constants):
            algorithm = environment.get_algorithm()
            assert type(algorithm) is Algorithm
            assert algorithm.id == constants["algorithm_id"]

        def test_environment_invoke(self, environment):
            problem = {"value": 70}

            res = environment.invoke(problem)
            assert res["out"] == 140

    class TestEnvironmentResourceCRUD:
        def test_create_environment_existing(self, client, constants):
            with pytest.raises(RequestError):
                AlgorithmEnvironment.create(
                    client.session,
                    constants["algorithm_id"],
                    constants["environment_name"],
                )

        def test_create_environment(self, client, constants):
            environment = AlgorithmEnvironment.create(
                client.session,
                constants["algorithm_id"],
                constants["environment_name_new"],
            )
            assert type(environment) is AlgorithmEnvironment
            assert environment.id == constants["environment_id_new"]

        def test_delete_environment(self, client, constants):
            environment = AlgorithmEnvironment(
                constants["environment_id_new"], session=client.session
            )
            res = environment.delete()
            assert res["id"] == constants["environment_id_new"]

        def test_delete_environment_error(self, client, constants):
            with pytest.raises(ResourceDeletionError):
                environment = AlgorithmEnvironment(
                    constants["environment_id"], session=client.session
                )
                environment.delete()

    class TestContext:
        @pytest.fixture(scope="class")
        def cclient(self, session, constants):
            os.environ["ALFA_CONTEXT"] = json.dumps(
                {
                    "alfaEnvironment": "dev",
                    "token": session.auth.token,
                    "environmentId": constants.get("environment_id"),
                }
            )
            client = AlgorithmClient()
            yield client
            del os.environ["ALFA_CONTEXT"]

        def test_context_info(self, cclient, constants):
            environment_id = cclient.session.context.get("environmentId")
            assert environment_id == constants["environment_id"]

        def test_get_environment(self, cclient, constants):
            environment = cclient.get_environment_from_context()
            assert type(environment) is AlgorithmEnvironment
            assert environment.id == constants["environment_id"]

        def test_get_algorithm(self, cclient, constants):
            algorithm = cclient.get_algorithm_from_context()
            assert type(algorithm) is Algorithm
            assert algorithm.id == constants["algorithm_id"]
