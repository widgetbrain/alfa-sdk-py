import pytest
from alfa_sdk import CacheClient
from unittest.mock import patch


class TestCache:
    @pytest.fixture(scope="class")
    def client(self, session):
        return CacheClient(session=session)

    @pytest.fixture(scope="class")
    def key(self):
        return "test"

    @pytest.fixture(scope="class")
    def value(self):
        return "test_value"

    @patch("alfa_sdk.common.session.parse_response")
    @patch("requests.sessions.Session.request")
    def test_store_value(self, mock_request, mock_parse, client, key, value):
        mock_request.return_value = {}
        mock_parse.return_value = {"key": "1:test", "value": value, "ttl": 3600}

        res = client.store_value(key, value)
        assert res["value"] == value

        mock_parse.assert_called_once()
        mock_request.assert_called_once()
        mock_request.assert_called_with(
            "post",
            "https://wb-core-development.widgetbrain.io/api/caches/test",
            json={"key": "test", "value": value, "ttl": 3600},
        )

    @patch("alfa_sdk.common.session.parse_response")
    @patch("requests.sessions.Session.request")
    def test_fetch_value(self, mock_request, mock_parse, client, key, value):
        mock_request.return_value = {}
        mock_parse.return_value = {"key": "1:test", "value": value}

        res = client.fetch_value(key)
        assert res["value"] == value

        mock_parse.assert_called_once()
        mock_request.assert_called_once()
        mock_request.assert_called_with(
            "get", "https://wb-core-development.widgetbrain.io/api/caches/test"
        )
