import os
import pytest
import json

from alfa_sdk import DataClient


class TestData:
    @pytest.fixture(scope="class")
    def client(self, session):
        return DataClient(session=session)

    @pytest.fixture(scope="class")
    def constants(self):
        return {
            "file_name": "alfa-sdk.json",
            "file_id": "5f8b396c-6695-4b58-aa0e-9b22b0cdc0ef",
            "file_keys": [
                "ModerationLabels",
                "fileName",
                "Labels",
                "Persons",
                "Celebrities",
            ],
        }

    @pytest.fixture(scope="function")
    def setup_teardown_update_data_file(self, session):
        # Setup
        data_file = session.request(
            "post",
            "data",
            "/api/Storages/upload",
            files={
                "file": (
                    "hello.txt",
                    open(os.path.join("tests", "data", "hello.txt"), "rb"),
                )
            },
        )
        # Execute test
        yield data_file

        # Teardown
        session.request("delete", "data", "/api/Storages/deleteFile/{}".format(data_file["id"]))

    def test_list_data_files(self, client, constants):
        files = client.list_data_files()
        file_ids = [file["id"] for file in files]
        file_names = [file["name"] for file in files]
        assert (constants["file_id"] in file_ids) is True
        assert (constants["file_name"] in file_names) is True

    def test_list_data_files_prefix(self, client, constants):
        prefix = constants["file_name"]
        files = client.list_data_files(prefix=prefix)
        assert all(file["name"] == prefix for file in files)

    def test_list_data_files_skip(self, client):
        skip = 2
        files_unskipped = client.list_data_files()
        files_skipped = client.list_data_files(skip=skip)
        assert all(
            files_unskipped[i]["id"] == files_skipped[i - skip]["id"]
            for i in range(skip, len(files_unskipped))
        )

    def test_list_data_file_limit(self, client):
        limit = 5
        files = client.list_data_files(limit=limit)
        assert len(files) <= limit

    def test_list_data_file_order(self, client):
        files = client.list_data_files(order="id DESC")
        ids = [file["id"].lower() for file in files]
        assert all(ids[i] >= ids[i + 1] for i in range(len(ids) - 1))

    def test_fetch_data_file(self, client, constants):
        res = client.fetch_data_file(constants["file_id"])
        data = json.loads(res)
        data_keys = data.keys()
        assert set(data_keys) == set(constants["file_keys"])

    def test_update_data_file(self, client, setup_teardown_update_data_file):
        # Arrange
        data_file = setup_teardown_update_data_file
        changes = {"name": "foo.txt"}

        # Act
        updated_file = client.update_data_file(data_file["id"], changes)

        # Assert
        assert updated_file["id"] == data_file["id"]
        assert updated_file["name"] == changes["name"]

        key_components = data_file["key"].split("/")[:-1]
        key_components.append(changes["name"])
        new_key = "/".join(key_components)
        assert updated_file["key"] == new_key

    def test_upload_data_file(self, client, session):
        # Arrange
        file_name = "hello.txt"
        content = open(os.path.join("tests", "data", "hello.txt"), "rb")

        # Act
        uploaded_file = client.upload_data_file(file_name, content)
        test_file = session.request("get", "data", "/api/Storages/download/{}".format(uploaded_file["id"]))
     
        # Assert
        assert uploaded_file["name"] == test_file["name"]
        assert uploaded_file["key"] == test_file["key"]

        # Teardown
        session.request("delete", "data", "/api/Storages/deleteFile/{}".format(uploaded_file["id"]))
