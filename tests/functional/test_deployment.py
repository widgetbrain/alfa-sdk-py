import pytest
import uuid

from alfa_sdk import AlgorithmClient
from alfa_sdk.resources import Release
from alfa_sdk.common.exceptions import ValidationError
from alfa_sdk.common.helpers import AlfaConfigHelper


class TestDeployment:
    @pytest.fixture(scope="class")
    def client(self, session):
        return AlgorithmClient(session=session)

    @pytest.fixture(scope="class")
    def constants(self):
        name = str(uuid.uuid4()).split("-")[0]

        return {
            "algorithm_id": "bb1d784a-bc4d-4f8a-9805-25d8a38baa29",
            "environment_id": f"9a882618-336e-4988-b85c-dc98b7e13c4f:bb1d784a-bc4d-4f8a-9805-25d8a38baa29:{name}",
            "environment_name": name,
            "environment_id_source": "9a882618-336e-4988-b85c-dc98b7e13c4f:bb1d784a-bc4d-4f8a-9805-25d8a38baa29:alfa-sdk",
        }

    @pytest.fixture(scope="class")
    def environment(self, client, constants):
        algorithm = client.get_algorithm(constants["algorithm_id"])
        environment = algorithm.create_environment(constants["environment_name"])

        yield environment

        environment.delete(force=True)

    class TestReleaseResource:
        @pytest.fixture(scope="class")
        def release(self, client, constants):
            environment = client.get_environment(constants["environment_id_source"])
            return environment.get_active_release()

        def test_release_info(self, release, constants):
            assert type(release) is Release
            assert release.environment_id == constants["environment_id_source"]
            assert release.status == "success"
            assert release.active is True

        def test_download(self, release):
            file = release.fetch_file()
            assert type(file) is bytes

    class TestDeployAlgorithm:
        @pytest.fixture(scope="class")
        def filepath(self, client, constants):
            environment = client.get_environment(constants["environment_id_source"])
            release = environment.get_active_release()

            path = "/tmp/alfa-sdk-deploy-test.zip"
            open(path, "wb").write(release.fetch_file())
            return path

        @pytest.fixture(scope="class")
        def configpath(self, filepath):
            file = AlfaConfigHelper.extract_from_package(filepath)

            path = "/tmp/alfa-sdk-deploy-test.yaml"
            open(path, "wb").write(file)
            return path

        def test_deploy_from_resource(self, environment, filepath, constants):
            release = environment.deploy("0.1.0", filepath)
            assert type(release) is Release
            assert release.environment_id == constants["environment_id"]
            assert release.version == "0.1.0"

        def test_deploy_from_client(self, client, filepath, constants):
            release = client.deploy(
                constants["algorithm_id"],
                constants["environment_name"],
                "0.1.0-copy",
                filepath,
            )
            assert type(release) is Release
            assert release.environment_id == constants["environment_id"]
            assert release.version == "0.1.0-copy"

        def test_deploy_existing(self, environment, filepath, constants):
            with pytest.raises(ValidationError):
                environment.deploy("0.1.0", filepath)

        def test_deploy_increment(self, environment, filepath, constants):
            release = environment.deploy("0.1.0", filepath, increment=True)
            assert type(release) is Release
            assert release.environment_id == constants["environment_id"]
            assert release.version == "0.1.1"

        def test_deploy_increment_not_necessary(self, environment, filepath, constants):
            release = environment.deploy("0.2.0", filepath, increment=True)
            assert type(release) is Release
            assert release.environment_id == constants["environment_id"]
            assert release.version == "0.2.0"

        def test_parse_conf_package(self, filepath):
            conf = AlfaConfigHelper.load(filepath, is_package=True)
            assert conf["id"] is not None
            assert conf["environment"] is not None
            assert conf["version"] is not None

        def test_parse_conf_file(self, configpath):
            conf = AlfaConfigHelper.load(configpath, is_package=False)
            assert conf["id"] is not None
            assert conf["environment"] is not None
            assert conf["version"] is not None
