import os
import pytest
import boto3
import pickle

from unittest.mock import patch, MagicMock

from alfa_sdk import AlgorithmClient
from alfa_sdk.resources import LocalMetaInstance, MetaUnit, MetaInstance
from alfa_sdk.common.exceptions import ResourceError

class TestMassCustomization:
    @pytest.fixture(scope="class")
    def client(self, session):
        return AlgorithmClient(session=session)

    @pytest.fixture(scope="class")
    def constants(self):
        return {
            "environment_id": "9a882618-336e-4988-b85c-dc98b7e13c4f:bb1d784a-bc4d-4f8a-9805-25d8a38baa29:alfa-sdk-meta",
            "tag": "alfasdk.functional.test",
            "unit_id": "9a882618-336e-4988-b85c-dc98b7e13c4f:alfasdk.functional.test",
            "file_name": "tests/data/demo-customer-1-total-items.pkl",
        }

    @pytest.fixture(scope="class")
    def unit(self, client, constants):
        return client.get_mass_customization_unit(constants["environment_id"], constants["tag"])

    #

    def test_get_unit_from_environment(self, client, unit, constants):
        environment = client.get_environment(constants["environment_id"])
        new_unit = environment.get_mass_customization_unit(constants["tag"])
        assert unit.id == new_unit.id

    def test_list_units(self, client, constants):
        units = client.list_mass_customization_units(constants["environment_id"])
        tags = [x["tag"] for x in units]
        assert (constants["tag"] in tags) is True

    class TestMetaUnit:
        def test_unit_info(self, unit, constants):
            assert type(unit) is MetaUnit
            assert unit.tag == constants["tag"]
            assert isinstance(unit.build_configurations, list) is True

        def test_list_instances(self, unit, constants):
            instances = unit.list_instances()
            unit_ids = [x["unitId"] for x in instances]
            matches = [(constants["tag"] in x) for x in unit_ids]
            assert all(matches) is True

        def test_store_activate_instance(self, unit, constants):
            stored_instance = unit.store_activate_instance(constants["file_name"])
            assert stored_instance["stored"] is True
            assert stored_instance["active"] is True
            assert stored_instance["algorithmEnvironmentId"] == constants["environment_id"]
            active_instance = unit.get_active_instance()
            assert active_instance.id == stored_instance["id"]

        def test_store_instance(self, unit, constants):
            stored_instance = unit.store_instance(constants["file_name"])
            assert "bucket" in stored_instance.keys()
            assert "key" in stored_instance.keys()
            s3 = boto3.resource("s3")
            s3.Object(stored_instance["bucket"], stored_instance["key"]).load()

        def test_store_instance_diskless(self, unit, constants):
            instance = {"foo": "bar"}
            dumped_instance = pickle.dumps(instance)
            stored_instance = unit.store_instance(dumped_instance)
            assert "bucket" in stored_instance.keys()
            assert "key" in stored_instance.keys()
            s3 = boto3.resource("s3")
            s3.Object(stored_instance["bucket"], stored_instance["key"]).load()

    class TestMetaUnitLocal:
        @pytest.fixture(scope="class", autouse=True)
        def setup_teardown(self):
            cwd = os.getcwd()
            os.chdir(os.path.join("tests", "data"))
            yield
            os.chdir(cwd)

        @pytest.fixture(scope="class")
        def local_client(self, session_meta_local):
            return AlgorithmClient(session=session_meta_local)

        @pytest.fixture(scope="class")
        def local_constants(self):
            return {
                "environment_id": "f7bae763-1be4-4b43-ad0d-852bd6f2a316:e46a9e3c-315f-45ea-9f1b-f3b7d4c59e57:foobar",
                "tag": "foo.bar.baz",
            }

        @pytest.fixture(scope="class")
        def local_unit(self, local_client, local_constants):
            return local_client.get_mass_customization_unit(
                local_constants["environment_id"],
                local_constants["tag"],
            )

        #

        def test_unit_info(self, local_unit, local_constants):
            assert isinstance(local_unit, MetaUnit)
            assert local_unit.tag == local_constants["tag"]
            assert local_unit.environment_id == local_constants["environment_id"]
            assert local_unit.local == True
            assert local_unit.build_configurations == []
            assert local_unit.watch_configurations == []

        def test_list_instances(self, local_unit):
            instances = local_unit.list_instances()
            assert instances == [
                {
                    "id": "1583234943",
                    "path": "./build/.instances/f7bae763-1be4-4b43-ad0d-852bd6f2a316/e46a9e3c-315f-45ea-9f1b-f3b7d4c59e57/foobar/foo.bar.baz/1583234943.pkl",
                },
                {
                    "id": "1583298780",
                    "path": "./build/.instances/f7bae763-1be4-4b43-ad0d-852bd6f2a316/e46a9e3c-315f-45ea-9f1b-f3b7d4c59e57/foobar/foo.bar.baz/1583298780.pkl",
                },
            ]

        def test_get_active_instance(self, local_unit):
            active_instance = local_unit.get_active_instance()
            assert isinstance(active_instance, LocalMetaInstance)
            assert (
                active_instance.path
                == "./build/.instances/f7bae763-1be4-4b43-ad0d-852bd6f2a316/e46a9e3c-315f-45ea-9f1b-f3b7d4c59e57/foobar/foo.bar.baz/1583298780.pkl"
            )

        def test_get_active_instance_no_file(self, local_client, constants):
            # Arrange
            local_unit_without_file = local_client.get_mass_customization_unit(
                constants["environment_id"], constants["tag"]
            )

            # Act
            active_instance = unit_no_file = local_unit_without_file.get_active_instance()

            # Assert
            assert isinstance(active_instance, MetaInstance)
            assert active_instance.environment_id == constants["environment_id"]
            assert active_instance.unit_id == constants["unit_id"]

        def test_active_instance_repr(self, local_unit):
            active_instance = local_unit.get_active_instance()
            assert active_instance.__repr__() == {}

        @patch("alfa_sdk.resources.meta.MetaUnit.get_active_instance_from_server", new=MagicMock(
            return_value=None
        ))
        def test_active_instance_no_active_instances(self, unit):
            with pytest.raises(ResourceError):
                unit.get_active_instance()

        def test_unit_info_exists_on_alfa(self, local_client, constants):
            # Act
            unit = local_client.get_mass_customization_unit(
                constants["environment_id"], constants["tag"]
            )

            # Assert
            assert isinstance(unit, MetaUnit)
            assert unit.tag == constants["tag"]
            assert isinstance(unit.build_configurations, list) is True
            assert len(unit.build_configurations) > 0

    #

    class TestMetaInstance:
        @pytest.fixture(scope="class")
        def instance(self, unit):
            return unit.get_active_instance()

        def test_instance_info(self, instance, constants):
            assert type(instance) is MetaInstance
            assert (constants["tag"] in instance.unit_id) is True
            assert instance.active is True

        def test_instance_download(self, instance):
            file = instance.fetch_file()
            assert type(file) is bytes

    #

    class TestLocalMetaInstance:
        @pytest.fixture(scope="class", autouse=True)
        def setup_teardown(self):
            cwd = os.getcwd()
            os.chdir(os.path.join("tests", "data"))
            yield
            os.chdir(cwd)

        @pytest.fixture(scope="class")
        def local_constants(self):
            return {
                "instance_id": "1583298780",
                "instance_path": "./build/.instances/f7bae763-1be4-4b43-ad0d-852bd6f2a316/e46a9e3c-315f-45ea-9f1b-f3b7d4c59e57/foobar/foo.bar.baz/1583298780.pkl",
            }

        @pytest.fixture(scope="class")
        def local_instance(self, local_constants):
            return LocalMetaInstance(local_constants["instance_path"])

        #

        def test_instance_info(self, local_instance, local_constants):
            assert local_instance.path == local_constants["instance_path"]

        def test_instance_download(self, local_instance):
            file = local_instance.fetch_file()
            assert isinstance(file, bytes)
            content = pickle.loads(file)
            assert content == 42
