import pytest
import os
import json

from alfa_sdk import IntegrationClient
from alfa_sdk.resources import Integration, IntegrationEnvironment
from alfa_sdk.common.exceptions import ResourceDeletionError, ValidationError


class TestIntegration:
    @pytest.fixture(scope="class")
    def client(self, session):
        return IntegrationClient(session=session)

    @pytest.fixture(scope="class")
    def constants(self, session):
        team_id = "9a882618-336e-4988-b85c-dc98b7e13c4f"
        tag = "alfa-sdk"
        environment = "multiply"
        new_environment = "alfa-sdk-new"

        return {
            "integration_id": f"{team_id}:{tag}",
            "environment_id": f"{team_id}:{tag}:{environment}",
            "environment_name": environment,
            "environment_id_new": f"{team_id}:{tag}:{new_environment}",
            "environment_name_new": new_environment,
        }

    @pytest.fixture(scope="class")
    def cclient(self, constants, session):
        os.environ["ALFA_CONTEXT"] = json.dumps(
            {
                "alfaEnvironment": "dev",
                "token": session.auth.token,
                "environmentId": constants.get("environment_id"),
                "userId": session.auth.user["userId"],
                "teamId": session.auth.user["teamId"],
            }
        )
        client = IntegrationClient()
        yield client
        del os.environ["ALFA_CONTEXT"]

    def test_list_integrations(self, client, constants):
        integrations = client.list_integrations()
        ids = [x["id"] for x in integrations]
        assert (constants["integration_id"] in ids) is True

    def test_list_environments(self, client, constants):
        environments = client.list_environments(constants["integration_id"])
        ids = [x["id"] for x in environments]
        assert (constants["environment_id"] in ids) is True

    def test_list_releases(self, client, constants):
        releases = client.list_releases(constants["environment_id"])
        environment_ids = [x["environmentId"] for x in releases]
        assert (constants["environment_id"] in environment_ids) is True

    class TestIntegrationResource:
        @pytest.fixture(scope="class")
        def integration(self, client, constants):
            return client.get_integration(constants["integration_id"])

        def test_integration_info(self, integration, constants):
            assert type(integration) is Integration
            assert integration.id == constants["integration_id"]
            assert integration.name == "ALFA SDK"

        def test_integration_environments(self, integration, constants):
            environments = integration.list_environments()
            names = [x["name"] for x in environments]
            assert (constants["environment_name"] in names) is True

        def test_integration_invoke(self, integration, constants):
            problem = {"value": 70}
            environment = constants["environment_name"]
            function_name = "double"

            res = integration.invoke(environment, function_name, problem)
            assert res["out"] == 140

    class TestIntegrationClient:
        def test_invoke(self, client, constants):
            integration = constants["integration_id"]
            environment = constants["environment_name"]
            function = "double"
            problem = {"value": 70}

            res = client.invoke(integration, environment, function, problem)
            assert res["out"] == 140

        def test_invoke_params_return_reference(self, client, constants):
            integration = constants["integration_id"]
            environment = constants["environment_name"]
            function = "double"
            problem = {"value": 70}

            res = client.invoke(
                integration, environment, function, problem, return_reference=True, parse=False
            )
            assert "returnReference=True" in res.request.url

        def test_invoke_params_return_holding_response(self, client, constants):
            integration = constants["integration_id"]
            environment = constants["environment_name"]
            function = "double"
            problem = {"value": 70}

            res = client.invoke(
                integration,
                environment,
                function,
                problem,
                return_holding_response=True,
                parse=False,
            )
            assert "returnHoldingResponse=True" in res.request.url

    class TestEnvironmentResource:
        @pytest.fixture(scope="class")
        def environment(self, client, constants):
            return client.get_environment(constants["environment_id"])

        def test_environment_info(self, environment, constants):
            assert type(environment) is IntegrationEnvironment
            assert environment.id == constants["environment_id"]
            assert environment.name == constants["environment_name"]
            assert environment.integration_id == constants["integration_id"]

        def test_environment_registrations(self, environment):
            registrations = environment.list_registrations()
            assert registrations.get("functions") is not None

        def test_get_integration(self, environment, constants):
            integration = environment.get_integration()
            assert type(integration) is Integration
            assert integration.id == constants["integration_id"]

        def test_environment_invoke(self, environment):
            problem = {"value": 70}
            function_name = "double"

            res = environment.invoke(function_name, problem)
            assert res["out"] == 140

    class TestEnvironmentResourceCRUD:
        def test_create_environment_existing(self, client, constants):
            with pytest.raises(ValidationError):
                IntegrationEnvironment.create(
                    client.session,
                    constants["integration_id"],
                    constants["environment_name"],
                )

        def test_create_environment(self, client, constants):
            environment = IntegrationEnvironment.create(
                client.session,
                constants["integration_id"],
                constants["environment_name_new"],
            )
            assert type(environment) is IntegrationEnvironment
            assert environment.id == constants["environment_id_new"]

        def test_delete_environment(self, client, constants):
            environment = IntegrationEnvironment(
                constants["environment_id_new"], session=client.session
            )
            res = environment.delete()
            assert res["id"] == constants["environment_id_new"]

        def test_delete_environment_error(self, client, constants):
            with pytest.raises(ResourceDeletionError):
                environment = IntegrationEnvironment(
                    constants["environment_id"], session=client.session
                )
                environment.delete()

    class TestContext:
        @pytest.fixture(scope="class")
        def cclient(self, session, constants):
            os.environ["ALFA_CONTEXT"] = json.dumps(
                {
                    "alfaEnvironment": "dev",
                    "token": session.auth.token,
                    "environmentId": constants.get("environment_id"),
                }
            )
            client = IntegrationClient()
            yield client
            del os.environ["ALFA_CONTEXT"]

        def test_context_info(self, cclient, constants):
            environment_id = cclient.session.context.get("environmentId")
            assert environment_id == constants["environment_id"]

        def test_get_environment(self, cclient, constants):
            environment = cclient.get_environment_from_context()
            assert type(environment) is IntegrationEnvironment
            assert environment.id == constants["environment_id"]

        def test_get_integration(self, cclient, constants):
            integration = cclient.get_integration_from_context()
            assert type(integration) is Integration
            assert integration.id == constants["integration_id"]
