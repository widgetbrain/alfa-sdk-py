# alfa-sdk

This package provides a Python SDK for developing algorithms using [ALFA](https://widgetbrain.com/product/).

## Installation

You can directly install alfa-sdk using [pip](http://www.pip-installer.org/en/latest/). This will install the alfa-sdk package as well as all dependencies.

```sh
$ pip install alfa-sdk
```

If you already have alfa-sdk installed and want to upgrade to the latest version, you can run:

```sh
$ pip install --upgrade alfa-sdk
```

## Development

To install requirements locally:

**1.** Activate local venv

```sh
$ virtualenv venv
$ source venv/bin/activate
```

**2.** Install requirements from setup.py

```sh
$ pip install -e ".[dev]"
```

## Changelog
- 0.1.52 (2022-04-11)
  - Add new services in endpoints list
  - Make keepalive optional
- 0.1.49-0.1.51 (2022-03-30)
  - Initialize session with keepalive
- 0.1.48 (2022-03-11)
  - Invoke an algorithm/integration with the run_options defined
  - Add upload_data_file method in DataClient
- 0.1.47 (2022-02-18)
  - Refactor credentials handling
  - Auth adjustments to match behaviour of alfa-sdk-js
- 0.1.46 (2022-02-09)
  - Clarify error message in get_active_instance when there are no instances
- 0.1.45 (2022-01-07)
  - Update AlgorithmClient.list_algorithms to use specified team_id
- 0.1.44 (2021-11-07)
  - Append neo sessionid to auth cookie
- 0.1.43 (2021-10-28)
  - Add support for setting default options in session
  - Add ability to set custom alfa environment in endpoint helper
- 0.1.42 (2021-10-19)
  - Expose method to just create Endpoint helper based on session
- 0.1.41 (2021-10-8)
  - Enable creating RequestError with custom status code
- 0.1.40 (2021-9-20)
  - Fix handling of team_id in algorithm & integrations resource
- 0.1.39 (2021-9-17)
  - Fix handling of integration_id in Integration Client
- 0.1.38 (2021-9-13)
  - Adjust Algorithm and Integration clients to support specified `team_id`
- 0.1.37 (2021-8-12)
  - Fetch configuration from config store when no other source of the configuration is available
- 0.1.36 (2021-5-28)
  - Improve error handling when error message is not a string
- 0.1.35 (2021-5-12)
  - Prioritize user-defined client id and client secret over cached token
- 0.1.34 (2021-4-5)
  - Prioritize credentials in parameters over context
- 0.1.33 (2021-3-23)
  - Remove None values from request parameters
- 0.1.32 (2021-3-19)
  - Use new quinyx domain (web-*.quinyx.com)
- 0.1.31 (2021-1-6)
  - Store alfa_id, alfa_env, and region on Session object
- 0.1.30 (2020-12-11)
  - Only throw TokenNotFoundError during authentication when neither a token nor cookie are found
- 0.1.29 (2020-11-24)
  - Added endpoints and resolve strategy for Quinyx Alfa
  - Added fetching of alfa_id and region
  - Added use of alfa_id and region to EndpointHelper, Authentication, and Session
- 0.1.28 (2020-11-13)
  - Added support for macaroon tokens specified in ALFA_CONTEXT to authenticate requests
- 0.1.26 (2020-10-9)
  - Added function argument to IntegrationClient.invoke and definition of function_type
- 0.1.25 (2020-9-29)
  - Added support for integrations
- 0.1.21 (?)
  - fetch data for Meta Unit from Alfa when it exists
- 0.1.20 (2020-3-24)
  - enabled fetching secret values of a team the client is allowed to access
- 0.1.19 (2020-3-12)
  - enabled the definition of the team_id of a client
- 0.1.18 (2020-3-9)
  - added Dataclient.update_data_file method
- 0.1.17 (2020-3-5)
  - added fallback mechanisms for local handling of MetaInstances when there is no file found locally
- 0.1.16 (2020-3-4)
  - added AlgorithmClient.get_context
  - added AlgorithmClient.get_active_instance_from_context
  - added local handling of MetaUnits and MetaInstances
- 0.1.15 (2020-2-19)
  - added prefix, skip, limit, and order arguments to list_data_files function
- 0.1.14 (2020-1-27)
  - replace deprecated secrets service
- 0.1.13 (2020-1-22)
  - add store_kpi function
- 0.1.12 (2020-1-09)
  - generalize errors according to alfa errors
  - handle errors based on error codes
- 0.1.11 (2019-9-02)
  - generalized auth tokens
- 0.1.10 (2019-7-15)
  - allow handling of instances without storing to disk
- 0.1.0 - 0.1.9 (2019-3-19)
  - initial version + bugfixes
